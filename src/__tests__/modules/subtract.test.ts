import { subtract } from '../../modules/subtract'

describe('subtract', () => {
  test('ok', () => {
    const data = [1, 2]
    const result = subtract(data[0], data[1])
    const expected = data[0] - data[1]
    expect(result).toBe(expected)
  })
})
